FROM node:latest AS builder
RUN apt-get update && apt-get install -y pngquant webp
COPY package*.json /build/
WORKDIR /build/
RUN npm ci
COPY . /build/
RUN npm run build

FROM nginx:latest
COPY --from=builder /build/dist /usr/share/nginx/html
