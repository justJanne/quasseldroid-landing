#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

image=${1}
output_dir=${2}
resolution=${3}
quality=${4}

convert "${image}.png" -colorspace RGB -filter Lanczos -distort Resize "${resolution}"x -colorspace sRGB "${output_dir}/${image}@${resolution}.png"
pngquant --quality "${quality}" "${output_dir}/${image}@${resolution}.png"
mv "${output_dir}/${image}@${resolution}-fs8.png" "${output_dir}/${image}@${resolution}.png"
