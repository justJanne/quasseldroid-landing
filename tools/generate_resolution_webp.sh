#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

cd "$(dirname "$(realpath -s "${0}")")"

image=${1}
output_dir=${2}
resolution=${3}

cwebp -lossless "${output_dir}/${image}@${resolution}.png" -o "${output_dir}/${image}@${resolution}.webp"
