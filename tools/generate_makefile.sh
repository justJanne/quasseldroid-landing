#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

QUALITY=85

cd "$(dirname "$(realpath -s "${0}")")"

OUT=../src/assets/images

RULES=()

function generate_rule() {
  IFS=' '

  image=${1}
  resolutions=${2}
  quality=${3}

  for resolution in ${resolutions}; do
    RULES+=("\$(IMAGE_OUT)/${image}@${resolution}.png")
    {
      echo "\$(IMAGE_OUT)/${image}@${resolution}.png: ${image}.png"
      echo "	mkdir -p \$(IMAGE_OUT)"
      echo "	\$(RESOLUTION) ${image} \$(@D) ${resolution} ${quality}"
    } >> $OUT/Makefile.wip
    
    RULES+=("\$(IMAGE_OUT)/${image}@${resolution}.webp")
    {
      echo "\$(IMAGE_OUT)/${image}@${resolution}.webp: \$(IMAGE_OUT)/${image}@${resolution}.png"
      echo "	\$(RESOLUTION_WEBP) ${image} \$(@D) ${resolution}"
    }  >> $OUT/Makefile.wip
  done
}

function finalize() {
  echo -n "all:" >> $OUT/Makefile
  for rule in "${RULES[@]}"; do
    echo -n " $rule" >> $OUT/Makefile
  done
  echo >> $OUT/Makefile
  cat $OUT/Makefile.wip >> $OUT/Makefile
  rm $OUT/Makefile.wip
}

cat <<EOF > $OUT/Makefile
ROOT = \$(shell realpath \$(shell pwd)/../../..)
OUT = \$(ROOT)/dist
IMAGE_OUT = \$(OUT)/assets/images
RESOLUTION = \$(ROOT)/tools/generate_resolution.sh
RESOLUTION_WEBP = \$(ROOT)/tools/generate_resolution_webp.sh
EOF
# Generate Images
themes=(material_light material_dark solarized_light solarized_dark gruvbox_light gruvbox_dark amoled dracula)
for image in "${themes[@]}"; do
  generate_rule "${image}" "220 266 335 352 532" "${QUALITY}"
done
generate_rule phone "220 300 400 520 640 800 1000 1220" "${QUALITY}"
generate_rule tablet "545 640 720 800 880 960 1090 1635 2180" "${QUALITY}"
generate_rule notifications "363 474 590" "${QUALITY}"
generate_rule desktop "363 474 590 726 948 1180" "${QUALITY}"

finalize
