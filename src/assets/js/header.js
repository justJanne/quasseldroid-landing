document.addEventListener("DOMContentLoaded", () => {
  const following_bar = document.querySelector(".following.bar");
  let callback = -1;
  const update_top_bar = function () {
    function clearCallback() {
      window.cancelAnimationFrame(callback);
      callback = -1;
    }
    if (callback > 0) clearCallback()
    callback = window.requestAnimationFrame(() => {
      following_bar.classList.toggle("qd", window.scrollY !== 0);
      clearCallback()
    })
  };

  if (document.body.classList.contains("index")) {
    document.addEventListener("scroll", update_top_bar);
    update_top_bar();
  }
});
