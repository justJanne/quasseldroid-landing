## Privacy Policy

To be able to provide you the best possible experience, and to avoid security threats, we need to collect and process certain information. The amount of information collected depends on which services you use:

### Contact via email

When you contact us via email we will collect the content of your email to be able to respond to your message. If your message contains technical information, which is not specific to you personally, e.g. a bug report, we will use this data and may publish it in our bug tracker to improve our software.

### Usage data

*Applies to: all services and websites on kuschku.de, k8r.eu, quasseldroid.info, justjanne.de*

When you visit our websites and services, we will store:

* Referrer (the website you visited us from)
* User-Agent (the browser and version you are using)
* Time and date of your visit
* IP address, including which country you are in
* Default Language configured in the browser

We store the full information for up to 2 weeks to be able to analyze security threats, after that only an anonymous summary of all visits will be stored.

### GitHub

*Applies to: github.com/justjanne*

If you interact with our projects on GitHub, the [privacy policy of GitHub](https://docs.github.com/en/github/site-policy/github-privacy-statement) applies.

### Services

*Applies to: all services and websites on kuschku.de, k8r.eu, quasseldroid.info, justjanne.de*

While using our services, we will store any and all data you enter, until you chose to delete the data or account. We may choose to delete accounts or data earlier.  
Data submitted while not logged in, if possible, are not linked to any account, and can not be removed.

Data will only be used for the service on which you entered it, and not be turned over to any third party, unless we are required to by law.

### Apps

*Applies to: Quasseldroid app*

Quasseldroid will only connect to servers configured by the user. Messages, user data, and other content are subject to the privacy policies of that server, and other connected servers.

Crash reports have to be manually submitted, in which case they are subject to the privacy policy for emails (see above).

### Further Questions

For further questions regarding our privacy policy, please contact [support@kuschku.de](mailto:support@kuschku.de).
